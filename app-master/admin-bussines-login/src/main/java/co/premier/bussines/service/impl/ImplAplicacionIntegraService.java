package co.premier.bussines.service.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.premier.bussines.service.IAplicacionIntegraService;
import co.premier.repository.IAplicacionIntegraRepository;
import co.premier.repository.entity.AplicacionIntegraEntity;

@Service
public class ImplAplicacionIntegraService implements IAplicacionIntegraService{

	@Autowired
	IAplicacionIntegraRepository appIntRepository;

	@Override
	public AplicacionIntegraEntity get(Long id) {
		return appIntRepository.findById(id).get();
	}

	@Override
	public List<AplicacionIntegraEntity> getAll() {
		return (List<AplicacionIntegraEntity>) appIntRepository.findAll();
	}

	@Override
	public void post(AplicacionIntegraEntity entity) {
		appIntRepository.save(entity);
	}

	@Override
	public void put(AplicacionIntegraEntity entity, long id) {
		appIntRepository.findById(id).ifPresent((x)->{
			entity.setId_app(id);
			appIntRepository.save(entity);
		});
		
	}

	@Override
	public Boolean delete(Long id) {
		appIntRepository.deleteById(id);
		return Boolean.TRUE;
	}	
}
