package co.premier.bussines.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.premier.bussines.service.IAutenticacionService;
import co.premier.repository.IAutenticacionRepository;
import co.premier.repository.entity.AutenticacionEntity;

@Service
public class ImplAutenticacionService implements IAutenticacionService{
	
	@Autowired
	IAutenticacionRepository autRepository;
	
	
	@Override
	public AutenticacionEntity get(Long id) {
		return autRepository.findById(id).get();
	}

	@Override
	public List<AutenticacionEntity> getAll() {
		return (List<AutenticacionEntity>) autRepository.findAll();
	}

	@Override
	public void post(AutenticacionEntity entity) {
		autRepository.save(entity);
	}

	@Override
	public void put(AutenticacionEntity entity, long id) {
		autRepository.findById(id).ifPresent((x)->{
			entity.setId_aut(id);
			autRepository.save(entity);
		});
		
	}

	@Override
	public Boolean delete(Long id) {
		autRepository.deleteById(id);
		return Boolean.TRUE;
	}

}
