package co.premier.bussines.service;

import java.util.List;

import co.premier.repository.entity.AplicacionIntegraEntity;

public interface IAplicacionIntegraService {

	 public AplicacionIntegraEntity get(Long id);
	 public List<AplicacionIntegraEntity> getAll();
	 public void post(AplicacionIntegraEntity entity);
	 public void put(AplicacionIntegraEntity entity, long id);
	 public Boolean delete(Long id);
	
}
