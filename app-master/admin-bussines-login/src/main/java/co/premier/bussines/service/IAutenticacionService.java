package co.premier.bussines.service;

import java.util.List;

import co.premier.repository.entity.AutenticacionEntity;


public interface IAutenticacionService {
	
	 public AutenticacionEntity get(Long id);
	 public List<AutenticacionEntity> getAll();
	 public void post(AutenticacionEntity entity);
	 public void put(AutenticacionEntity entity, long id);
	 public Boolean delete(Long id);

}
