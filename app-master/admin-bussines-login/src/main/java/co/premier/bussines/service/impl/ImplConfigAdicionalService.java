package co.premier.bussines.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.premier.bussines.service.IConfigAdicionalService;
import co.premier.repository.IConfigAdicionalRepository;
import co.premier.repository.entity.ConfigAdicionalEntity;

@Service
public class ImplConfigAdicionalService implements IConfigAdicionalService{

	@Autowired
	IConfigAdicionalRepository configRepository;
	
	@Override
	public ConfigAdicionalEntity get(Long id) {
		return configRepository.findById(id).get();
	}

	@Override
	public List<ConfigAdicionalEntity> getAll() {
		return (List<ConfigAdicionalEntity>) configRepository.findAll();
	}

	@Override
	public void post(ConfigAdicionalEntity entity) {
		configRepository.save(entity);
	}

	@Override
	public void put(ConfigAdicionalEntity entity, long id) {
		configRepository.findById(id).ifPresent((x)->{
			entity.setId_conf(id);
			configRepository.save(entity);
		});
	}

	@Override
	public Boolean delete(Long id) {
		configRepository.deleteById(id);
		return Boolean.TRUE;
	}

}
