package co.premier.bussines.service;

import java.util.List;

import co.premier.repository.entity.ConfigAdicionalEntity;


public interface IConfigAdicionalService {
	
	 public ConfigAdicionalEntity get(Long id);
	 public List<ConfigAdicionalEntity> getAll();
	 public void post(ConfigAdicionalEntity entity);
	 public void put(ConfigAdicionalEntity entity, long id);
	 public Boolean delete(Long id);

}
