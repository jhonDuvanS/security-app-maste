package co.premier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.premier.repository.entity.AplicacionIntegraEntity;

@Repository
public interface IAplicacionIntegraRepository extends CrudRepository<AplicacionIntegraEntity, Long>, JpaRepository<AplicacionIntegraEntity, Long>{
	
	/**
	 * 
	 * @param codigo Es el codigo de la aplicación.
	 * @return
	 */
	AplicacionIntegraEntity findAllByCode();
	
}
