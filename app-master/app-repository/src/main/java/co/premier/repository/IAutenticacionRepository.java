package co.premier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import co.premier.repository.entity.AutenticacionEntity;

public interface IAutenticacionRepository extends CrudRepository<AutenticacionEntity, Long>, JpaRepository<AutenticacionEntity, Long>{

}
