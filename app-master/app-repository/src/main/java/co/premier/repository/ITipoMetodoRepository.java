package co.premier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import co.premier.repository.entity.TipoMetodoEntity;

public interface ITipoMetodoRepository extends CrudRepository<TipoMetodoEntity, Long>, JpaRepository<TipoMetodoEntity, Long>{

}
