package co.premier.repository.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Entity
@Table(name = "app_aplicacion_integra")
@NamedQueries({
	@NamedQuery(name = "AplicacionIntegraEntity.findAllByCode", query = "select aplicacionIntegraEntity FROM AplicacionIntegraEntity aplicacionIntegraEntity")
})
public class AplicacionIntegraEntity extends Auditoria<String>{
	
	@Id
	@Column(name = "id_app")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_app;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "codigo")
	private String codigo;
	
	@OneToMany(mappedBy = "aplicacion", cascade = CascadeType.ALL)
	private List<AutenticacionEntity> autenticaciones;

	@OneToMany(mappedBy = "aplicacion", cascade = CascadeType.ALL)
	private List<ConfigAdicionalEntity> configuracionesAdic;
	
	@OneToMany(mappedBy = "aplicacion", cascade = CascadeType.ALL)
	private List<ServicioEntity> servicios;
}
