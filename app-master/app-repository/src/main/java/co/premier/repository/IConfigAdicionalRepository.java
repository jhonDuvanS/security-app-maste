package co.premier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import co.premier.repository.entity.ConfigAdicionalEntity;

public interface IConfigAdicionalRepository extends CrudRepository<ConfigAdicionalEntity, Long>, JpaRepository<ConfigAdicionalEntity, Long> {

}
