package co.premier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import co.premier.repository.entity.ServicioEntity;

public interface IServicioRepository extends CrudRepository<ServicioEntity, Long>, JpaRepository<ServicioEntity, Long>{

}
