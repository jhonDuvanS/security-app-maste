package co.premier.loginrest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.premier.bussines.service.IAplicacionIntegraService;
import co.premier.repository.entity.AplicacionIntegraEntity;

@RestController
@RequestMapping("v1.0/aplicaciones")
public class AplicacionRest {
	
	@Autowired
	IAplicacionIntegraService appService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public AplicacionIntegraEntity findAllByCode(){
		return findAllByCode();
	}
}
