package co.premier.adminrest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v.1/test")
public class RestTest {
	
	@RequestMapping(value = "/",  method = RequestMethod.GET )
	public ResponseEntity<String> hello(){
		return new ResponseEntity<String>("Ok",HttpStatus.OK);
	}
	
}
